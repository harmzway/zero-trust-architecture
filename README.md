# Zero Trust Architecture [Development in Progress]

The Zero Trust Architecture project is a unified composite of open source and free cloud resources connected in a way that allows for secure application access any time, anywhere - no matter if the application is in your private network or in the cloud.

This architecture lays a generic, secure foundation for nearly any application you would like to run within it. By following the sequence of steps below, you will be guided through a complete setup procedure for the Zero Trust architecture, as well as examples of how to utilize it with modern cloud and on-premise resources.  

Our team at Open Energy Project works full-time to create, update, improve, and iterate this framework, as we utilize it for our business applications. We wanted to share this because we believe it can help others reduce risk through security, learn and utilize modern technology stacks, and balance extremely low cost (a domain name is the only cost) with rich, powerful feature sets.

Feel free to check us out at https://openenergyproject.org to watch us grow and learn more about our main project.

# Why Zero Trust?

Zero Trust (also known as BeyondCorp) is a security model utilized by industry leaders for securing an ever-growing set of decentralized users and applications. Feel free to explore why and how this model is being implemented from these security champions:

* Google: https://cloud.google.com/beyondcorp
* Cloudflare: https://www.cloudflare.com/learning/security/glossary/what-is-zero-trust/
* Gitlab: https://about.gitlab.com/handbook/security/

# Features

* Enterprise-grade, secure platform accessibility and management from anywhere, any time (utilizes Cloudflare + Google Identity + NGINX + Gitlab)
* Easy container management (utilizes Portainer)
* Versatile CI/CD (utilizes Gitlab)
* Simple container disaster recovery (utilizes Gitlab)

# How to Support Development

* Donate Development Effort: Send us a private message here on GitLab
* Donate BTC: 1JbNF8gZ3Zf7d43EuF6iRxYKu4TvwnMWkN
* Donate ETH: 0x5975090E15EC1AEe23B5eFb9aA19203d4467d092
* Install it, use it, then share your experience and ideas with us

# Architecture Overview

* To prioritize security & privacy; we follow a combination of Google, Github, and Cloudflare zero trust philosophies and free-tier applications.
* To work on any platform, plan for scalability, and stay lightweight: we have decided to utilize Docker for our on-premise applications.
* In an effort to ensure all of this is free (aside from a domain name), we pursue a never-ending quest of seeking, testing, and improving to ensure we utilize the systems most capable of accomplishing our mission.

![Zero Trust Architecture](img/ZeroTrustArchitecture.png "BeyondCorp and Zero Trust are the same thing")

# Setup Overview

The following mix of cloud and on-premise systems are utilized to set up your own instance of the Zero Trust Architecture. This section explains the reasoning behind using each platform and provides a link to the wiki page that will walk you through each step of the setup procedure. Depending on your experience and focus level, this can take anywhere from a couple hours to a full day.

Be sure to follow the steps from top to bottom to completion, as each section builds on the foundation of the previous steps.

1. Google Cloud Identity and Google Domains

* Purpose (Google Cloud Identity) Enables secure single sign on (SSO) capabilities across all components of OEP through a created Google Identity (example@yourdomain.com)
* Purpose (Google Domains) - Google Domains provide some unique capabilities that easily integrate with Google Identity and Cloudflare services.
* More info (Google Cloud Identity) - https://cloud.google.com/identity
* More info (Google Domains) - https://domains.google/
* Walkthrough: [Google Cloud Identity and Google Domains: Initial Setup and Config](https://gitlab.com/symbiontik/zero-trust-architecture/-/wikis/Google-Cloud-Identity-and-Google-Domains:-Initial-Setup-and-Config)

2. Cloudflare

* Purpose - Provides additional secure SSO, advanced DNS, proxy, security analysis/reporting, and other edge computing features.
* More info (Cloudflare DNS) - https://support.cloudflare.com/hc/en-us/articles/360017421192-Cloudflare-DNS-FAQ
* More info (Cloudflare Services) - https://www.cloudflare.com/
* Walkthrough: [Cloudflare: Initial Setup and Config](https://gitlab.com/symbiontik/zero-trust-architecture/-/wikis/Cloudflare:-Initial-Setup-and-Config)

3. Gitlab

* Purpose - Simple, streamlined mechanisms for code deployment, Docker image storage, and backup capabilities.
* More info - https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/
* Walkthrough: [Gitlab: Initial Setup and Config](https://gitlab.com/symbiontik/zero-trust-architecture/-/wikis/Gitlab:-Initial-Setup-and-Config)
* Note: Include Access Token walkthrough in Wiki Document
* Note: Include Cloudflare Integration with direct Oauth SSO capabilities.

4. Local Server

* Purpose - Acts as a bridge between on-premise energy monitoring hardware and cloud resources.
* More info (Reverse Proxy) - https://www.cloudflare.com/learning/cdn/glossary/reverse-proxy/
* Walkthrough: [Local Server: Initial Setup and Config](https://gitlab.com/symbiontik/zero-trust-architecture/-/wikis/Local-Server:-Initial-Setup-and-Config)
* Notes: For zero-trust-architecture project, include Portainer "Easy Docker App Launch" capabilities

5. Local Router

* Purpose - Allows for secure communications from cloud services to local server ports.
* More info - https://www.noip.com/support/knowledgebase/general-port-forwarding-guide/
* Walkthrough: [Local Router: Initial Setup and Config](https://gitlab.com/symbiontik/zero-trust-architecture/-/wikis/Local-Router:-Initial-Setup-and-Config)

6. Analytics and Logging

* Purpose - Provides detailed statistics, metrics, and other data relevant to the health and performance of your complete environment.
* More info (Google) - https://support.google.com/a/answer/9725452?hl=en&ref_topic=9027054
* More info (Cloudflare DNS) - https://www.cloudflare.com/insights/
* More info (Cloudflare Gateway) - https://teams.cloudflare.com/gateway/
* Walkthrough: [Link to Analytics Wiki Document]

7. Free Services, Resources, and Tools
* Purpose - Provides an ever-growing list of open-source, free, and/or other creative resources
* More info - [Continuously updated list of free resources](https://github.com/ripienaar/free-for-dev)

8. Secure Cloud Application Access
* Purpose - Provide an example secure cloud application access workflow using Cloudflare Access and Grafana(or InfluxDB)
* More info (Cloudflare) - https://developers.cloudflare.com/access/setting-up-access/securing-applications/
* ?More info? (Google) - https://cloud.google.com/identity/solutions/enable-sso
* Walkthrough: [Link to _______ SSO Wiki Document]

9. Gitlab: Advanced Features
* Objective: Setup advanced features such as Gitlab Container registry integration (for disaster recovery or system host transfer).
* Reference: https://docs.gitlab.com/ee/user/packages/container_registry/
* Walkthrough: [Link to Gitlab Advanced Features Wiki Document]

10. Coding Environment
* Objective: Setup and configure a coding environment on your computer to streamline code changes and easily incorporate changes from your CI/CD pipelines.
* Reference: https://junaidshahid.com/gitlab-integration-with-visual-studio-2019/
* Notes (add to wiki): To easily customize your code repository, you're going to need a development environment. We HIGHLY recommend Visual Studio Code due the the simplicity of setup - follow the sequence of screenshots below to setup Git, Visual Studio Code, and a connection to your Gitlab project:
* Walkthrough: [Link to Coding Environment Wiki Document]

# License

Open Energy Project is licensed under Eclipse Public License 2.0

# Gratitude

* Google's BeyondCorp Security Team
* Google's team and their suite of free applications
* Cloudflare's creator, Lee Holloway
* Cloudflare's team and their suite of free applications
* Gitlab's team and their suite of free applications
* NGINX's team and their free, open-source application
* Portainer's team and their free, open-source application

# Change, Progress, and Collaboration
We believe we've only put together a foundation for what this could become. 
With your insights, ideas, and passion, we have the power to change the way the world thinks about energy.

Thank you all :)
-sym